"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var HelloComponent = /** @class */ (function () {
    function HelloComponent(helloService) {
        this.helloService = helloService;
    }
    HelloComponent.prototype.func1 = function () {
        return "some value changed";
    };
    return HelloComponent;
}());
exports.HelloComponent = HelloComponent;
//# sourceMappingURL=hello.component.js.map