"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var hello_component_1 = require("./hello.component");
var MockHelloService = /** @class */ (function () {
    function MockHelloService() {
    }
    MockHelloService.prototype.sayHello = function () {
        return "Hello world!";
    };
    return MockHelloService;
}());
describe("HelloComponent", function () {
    it("should say 'Hello world!'", function () {
        var mockHelloService = new MockHelloService();
        var helloComponent = new hello_component_1.HelloComponent(mockHelloService);
        expect(helloComponent.func1()).toEqual("some value");
    });
});
//# sourceMappingURL=hello.component.spec.js.map