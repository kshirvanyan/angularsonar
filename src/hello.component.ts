import { IHelloService } from "./hello.service.interface";

export class HelloComponent {

    constructor(private helloService: IHelloService) {}
    public func1(): string {
        return "some value";
    }
    public func2(): string {
        return "some value";
    }
    public func3(): string {
        return "some value";
    }
    public func4(): string {
        return "some value";
    }
    public func5(): string {
        return "some value";
    }
    public func6(): string {
        return "some value";
    }
    public func7(): string {
        return "some value";
    }
}
